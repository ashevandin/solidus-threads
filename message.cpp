#include "message.h"

using namespace solidus_threads;

message::message(int64_t thread_id, size_t message_id): thread_id_{thread_id}, id_{message_id}
{

}

std::ostream& solidus_threads::operator<< (std::ostream& o, const solidus_threads::message& m)
{
	o << "ThreadID:" << m.thread_id_ << " MsgID:" << m.id_;
	return o;
}

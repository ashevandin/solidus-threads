#include <iostream>
#include <exception>
#include "pool.h"

int main(int argc, char **argv) {
	try {
		solidus_threads::pool p { };

		std::cout << "solidus threads: start" << std::endl;
		p.spawn(6);
		p.join();
		std::cout << "solidus threads: finish" << std::endl;
	} catch (const std::exception &e) {
		std::cerr << "exception: " << e.what() << std::endl;
	}

	return 0;
}

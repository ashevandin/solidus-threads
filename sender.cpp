#include <thread>

#include "sender.h"
#include "pool.h"
using namespace solidus_threads;

std::atomic_int64_t sender::thread_id_{};

sender::sender(pool& p): pool_{p} {

}

std::thread sender::send(size_t messages_number) {
	auto send_worker =
	   [this](int64_t thread_id, size_t messages_number) ->void {

		for (size_t message_i = 0; message_i < messages_number; message_i++ ) {
			message m{thread_id, message_i};
			pool_.message_send(m);
		}

	   size_t senders_spawn_nmb {0};
	   if(20 <= pool_.senders_nmb()) {
		   if(0 == thread_id%3) {
			  senders_spawn_nmb = 1;
		   } else if (0 == thread_id%7) {
			   senders_spawn_nmb = 2;
		   }
	   }

	   if(senders_spawn_nmb) {
		   pool_.spawn(senders_spawn_nmb);
	   }
	};
	return std::thread{send_worker, ++thread_id_, messages_number};
}



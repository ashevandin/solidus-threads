#include <utility>
#include <iostream>
#include <chrono>
#include <cstdlib>
#include <ctime>
#include <iterator>
#include <utility>

#include "pool.h"

using namespace solidus_threads;

pool::pool() :
		workers_nmb_ { 0 },
		messages_to_process_nmb_ { 0 } {
	srand(time(0));

	std::future<void> terminate_sig = messages_process_terminate_.get_future();
	messages_process_worker_ = std::thread { &pool::messages_process, this,
			std::move(terminate_sig) };
}

pool::~pool() {
	messages_process_terminate();
}

void pool::spawn(size_t senders_number) {
	workers_t workers_for_senders { senders_number };
	for (size_t i = 0; i < senders_number; ++i) {
		sender s { *this };
		const auto messages_send_number = (rand() % messages_max_send_) + 1;
		auto t = s.send(messages_send_number);
		workers_for_senders[i] = std::move(t);
		workers_nmb_++;
	}

	std::lock_guard<std::mutex> lock { workers_m_ };
	workers_.insert(workers_.end(),
			std::make_move_iterator(workers_for_senders.begin()),
			std::make_move_iterator(workers_for_senders.end()));
}

void pool::join() {
	while (workers_nmb_) {
		workers_t workers_to_join { };
		{
			std::lock_guard<std::mutex> lock { workers_m_ };
				workers_to_join.insert(workers_to_join.end(),
						std::make_move_iterator(workers_.begin()),
						std::make_move_iterator(workers_.end()));
		}

		for (auto &t : workers_to_join) {
			if (t.joinable()) {
				t.join();
				workers_nmb_--;
			}
		}
	}

	messages_process_terminate();
}

size_t pool::senders_nmb() const {
	return static_cast<size_t>(workers_nmb_);
}

void pool::message_send(const message m) {
	const std::lock_guard<std::mutex> lock { messages_q_m_ };
	messages_.push_back(m);
	messages_to_process_nmb_++;
	is_message_ready_ = true;
	message_ready_.notify_one();
}

void pool::messages_process(std::future<void> terminate_sig) {
	bool continue_process { true };
	while (continue_process) {
		std::vector<message> messages_to_process { };

		{
			std::unique_lock<std::mutex> lock { messages_q_m_ };
			message_ready_.wait(lock, [this] {return is_message_ready_;});
			messages_to_process.swap(messages_);
			is_message_ready_ = false;
		}

		// process the messages
		for (const auto m : messages_to_process) {
			std::cout << m << std::endl;
			messages_to_process_nmb_--;
		}

		continue_process = 0 == messages_to_process_nmb_ && terminate_sig.wait_for(std::chrono::milliseconds(1))
					== std::future_status::timeout;
	}
}

void pool::messages_process_terminate() {
	if (messages_process_worker_.joinable()) {
		messages_process_terminate_.set_value();
		messages_process_worker_.join();
	}
}

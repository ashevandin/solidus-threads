#ifndef SENDER_H_
#define SENDER_H_

#include <atomic>

namespace solidus_threads {
class pool;
class sender {
public:
	friend class pool;
private:
	sender(pool&);
	std::thread send(size_t);

	static std::atomic_int64_t thread_id_;
	pool& pool_;
};
}




#endif /* SENDER_H_ */

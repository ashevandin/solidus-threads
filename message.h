#ifndef MESSAGE_H_
#define MESSAGE_H_

#include <ostream>

namespace solidus_threads {
class message {
public:
	message(int64_t, size_t);

	friend std::ostream& operator<< (std::ostream&, const message&);
private:
	const int64_t thread_id_;
	const size_t id_;
};
std::ostream& operator<< (std::ostream&, const message&);

}

#endif /* MESSAGE_H_ */

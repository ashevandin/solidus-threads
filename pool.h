#ifndef POOL_H_
#define POOL_H_
#include <vector>
#include <thread>
#include <mutex>
#include <future>
#include <functional>
#include <condition_variable>
#include <atomic>

#include "sender.h"
#include "message.h"

namespace solidus_threads {

	class pool {
	public:
		pool();
		~pool();
		void join();
		void spawn(size_t);
		void message_send(const message);
		size_t senders_nmb() const;

		static constexpr size_t messages_max_send_ = 100;
	private:
		using workers_t = std::vector<std::thread>;
		using workers_size_t = std::atomic<workers_t::size_type>;
		using messages_q_t = std::vector<message>;
		using messages_size_t = std::atomic<messages_q_t::size_type>;

		void messages_process(std::future<void>);
		void messages_process_terminate();

		workers_t workers_;
		std::mutex workers_m_;
		workers_size_t workers_nmb_;

		messages_q_t messages_;
		bool is_message_ready_ = false;
		std::condition_variable message_ready_;
	    std::mutex messages_q_m_;
	    std::thread messages_process_worker_;
	    std::promise<void> messages_process_terminate_;
	    messages_size_t messages_to_process_nmb_;
	};


}



#endif /* POOL_H_ */
